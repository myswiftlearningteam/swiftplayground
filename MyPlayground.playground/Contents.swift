//: Playground - noun: a place where people can play

import UIKit

// var - variable , let - constant
var str = "Hello, Aby Mathew"
let usDollarToINR = 67

// If the initial value doesn’t provide enough information (or if there is no initial value), specify the type by writing it after the variable, separated by a colon.

var name : String = "Aby Mathew"
let inrRate : Float = 67.00;

// Values are never implicitly converted to another type. If you need to convert a value to a different type, explicitly make an instance of the desired type.

let message = "Current Dollar Rate is "
let inrDollar = 67.00
let label = message + String(inrDollar) // Here we explicitly converting our inrDollar float value to string


//There’s an even simpler way to include values in strings: Write the value in parentheses, and write a backslash (\) before the parentheses. For example:

let usDollar = 67.00
let pound = 88.72
let currentRates = "us dollar value \(usDollar) pound value \(pound) total value -- \(usDollar + pound)"

// Create arrays and dictionaries using brackets ([]), and access their elements by writing the index or key in brackets. A comma is allowed after the last element.

//-----Array
var iosDevelopers = ["Sinson","Stephy","Karthik","Vishnu Vijayan"]
print(iosDevelopers[3])
iosDevelopers.append("Siraj") // adding new element into array

// --- DICTIONARY
var developers = [".Net" : "Deen","php" : "Mijoe","iOS" : "Sunith","Android" : "Praveen"];
let keysName = Array(developers.keys);
let values = Array(developers.values);
developers["php"] = "Sobin"

//If type information can be inferred, you can write an empty array as [] and an empty dictionary as [:]—for example, when you set a new value for a variable or pass an argument to a function.

var developersNames = []
var designations = [:]
designations = ["test" : "value"]


// --------- control flow 

let points = [43,23,56,75,4]
var teamPoint : Int = 0
for point in points {
    
    if point > 50 {
        teamPoint += point
    }else {
        print("point less")
    }
}
print(teamPoint)


//-- Optional values (?)
/*
You can use if and let together to work with values that might be missing. These values are represented as optionals. An optional value either contains a value or contains nil to indicate that a value is missing. Write a question mark (?) after the type of a value to mark the value as optional.
If the optional value is nil, the conditional is false and the code in braces is skipped. Otherwise, the optional value is unwrapped and assigned to the constant after let, which makes the unwrapped value available inside the block of code.
*/

let firstTeam : String? = "macOsSierra"
var greeting : String?
if let teamName = firstTeam {
    
    greeting = teamName
}else {
    greeting = "else case"
}
print(greeting)


//Another way to handle optional values is to provide a default value using the ?? operator. If the optional value is missing, the default value is used instead.

let firstTeamName : String? = "Argentina"
let secondTeamName : String?
let finalMessage = "Final match between \(firstTeamName ?? secondTeamName)"

//-- SWITCHES --- 

//Switches support any kind of data and a wide variety of comparison operations—they aren’t limited to integers and tests for equality.

let person = "aby"
switch person {
    
    case "mathew":
        print("last name")
    case "Aby":
        print("Caps name")
    case "aby":
        print("FIRST NAME")
    default:
        print("default")
}


// - for in loop iteration 

let dict = [

    "FirstTeam" : [23,45,57,97],
    "SecondTeam" : [78,66,92,107],
]

var largeNumber = 0
var kindValue : String? = nil
for (kind,pointsValue) in dict{
    for pointObj in pointsValue {
        
        if pointObj > largeNumber {
            largeNumber = pointObj
            kindValue = kind
        }
    }
}
print(kindValue)

// Use ..< to make a range that omits its upper value, and use ... to make a range that includes both values.
for i in 1...10 {
    print(i)
}

//-------- FUNCTION ----

func welcomeMessage(person :String , message:String) -> String {
    
    return "Hi \(person). your message is - \(message)"
}

welcomeMessage("Aby", message: "Colour Grading")

// TUPLE

func ageCalculation(ages: [Int]) -> (minAge: Int, maxAge: Int, totalAge: Int) {
    
    var age1 = ages[0]
    var age2 = ages[0]
    var totalAge = 0
    for ageObj in ages {
        
        if ageObj > age1 {
            
            age1 = ageObj // Maximum age
        }else if ageObj < age2 {
            
            age2 = ageObj // Minimum age
        }
        totalAge += ageObj;
    }
    return (age2,age1,totalAge)
}

var ageLable = ageCalculation([23,4,12,55,59,70,65]);
ageLable.totalAge
ageLable.minAge
ageLable.maxAge

// AVERAGE FINDING METHOD

func calculateAverageAge(ageArray: Int...) -> Float {
    
    var totalAgeValue = 0
    for ageObject in ageArray {
        
        totalAgeValue += ageObject
        
    }
    
   return Float(totalAgeValue / ageArray.count)
}

var averageValue = calculateAverageAge(24,34,56)
averageValue


//-- Nested Function -- 

func getTotal() -> Int {
    
    var y = 10
    
    func add() {
        y *= 4
    }
    
    add()
    return y;
}

getTotal()

//-- Function with another function as return type

func increamentorFunction() -> ((Int) ->Int) {
    
    func incrementor(baseValue: Int) -> Int {
        
        return baseValue + 1
        
    }
    return incrementor
}

var increment = increamentorFunction()
increment(50)

//---  Function with another function as parameter.

func isContainAnyEvenNumber(arrayList:[Int] , condition:(Int -> Bool)) -> Bool {
    
    for itemObj in arrayList {
        
        if (condition(itemObj)) {
            
            return true
        }
    }
    return false
}

func isEvenNumber (number:Int) -> Bool {
    
    let modvalue = number % 2
    if modvalue == 0 {
        return true
    }
    return false
}

var numberArray = [23,31,41,5];
isContainAnyEvenNumber(numberArray, condition: isEvenNumber)
 


// ----- CLASS AND OBJECTS -------/

class Shape {
    var numOfSides : Int = 0
    var name : String? = nil
  
    // Initializer
    init (name : String ) {
        self.name = name;
    }

    func description() -> String {
        return "This is a \(name) with number of sides \(numOfSides)"
    }
}

//Create an instance of a class by putting parentheses after the class name. Use dot syntax to access the properties and methods of the instance.

var shape = Shape(name: "BASIC SHAPE")
shape.numOfSides = 1
shape.description()


// Experiment
class Circle: Shape {
    
    var radius : Float
    // Cirlce Initializer method
    init(radius:Float, name: String) {
        self.radius = radius
        super.init(name: name)
    }
    
    // Circle area
    func area() -> Float {
        return 3.14 * (self.radius * self.radius)
    }
    
    override func description() -> String {
        return "This is a \(name) with Area : \(area())"
    }
    
}

var circle = Circle(radius: 5, name: "Circle")
circle.description()

class EquilateralTriagle: Shape {
    
    var sideLength : Double = 0.0
    
    init(sideLength : Double,name: String) {
        self.sideLength = sideLength
        super.init(name: name)
        numOfSides = 3
    }
    
    //----- Adding Setter and Getter methods for a property "Perimeter"

    var perimeter : Double {
        
        get{
            return 3 * sideLength
        }
        set (perimeterValue){
            sideLength = perimeterValue / 3.0
        }
    }
    
    override func description() -> String {
        return "This is \(name) with number of sides \(numOfSides)"
    }
    
}

var equilateral = EquilateralTriagle(sideLength: 4, name: "EQUILATERAL")
equilateral.name
equilateral.perimeter
equilateral.perimeter = 21



// -- ENUMERATIONS AND STRUCTURES -----

enum Rank: Int {
    case ace = 1
    case two, three, four, five, six, seven, eight, nine, ten
    case jack, queen, king
    func simpleDescription() -> String {
        switch self {
        case .ace:
            return "ace"
        case .jack:
            return "jack"
        case .queen:
            return "queen"
        case .king:
            return "king"
        default:
            return String(self.rawValue)
        }
    }
}
let ace = Rank.ace
let aceRawValue = ace.rawValue

if let convertedRank = Rank(rawValue: 3) {
    let threeDescription = convertedRank.simpleDescription()
}



//  The case values of an enumeration are actual values, not just another way of writing their raw values.

enum Suit : Int {
    case spades, hearts, diamonds, clubs
    func simpleDescription() -> String {
        switch self {
        case .spades:
            return "spades"
        case .hearts:
            return "hearts"
        case .diamonds:
            return "diamonds"
        case .clubs:
            return "clubs"
        }
    }
    
    //Experiment: Add a color() method to Suit that returns “black” for spades and clubs, and returns “red” for hearts and diamonds.
    func colour() -> String {
        switch self {
        case .spades:
            return "Black"
        case .hearts:
            return "Red"
        case .diamonds:
            return "Red"
        case .clubs:
            return "Black"
        }
    }
}

let hearts = Suit.hearts
let heartsDescription = hearts.simpleDescription()

struct Card {
    var rank: Rank
    var suit: Suit
    func simpleDescription() -> String {
        return "The \(rank.simpleDescription()) of \(suit.simpleDescription())"
    }
}
let threeOfSpades = Card(rank: .three, suit: .spades)
let threeOfSpadesDescription = threeOfSpades.simpleDescription()

//Experiment: Add a method to Card that creates a full deck of cards, with one card of each combination of rank and suit.

/* An instance of an enumeration case can have values associated with the instance. Instances of the same enumeration case can have different values associated with them. You provide the associated values when you create the instance. Associated values and raw values are different: The raw value of an enumeration case is the same for all of its instances, and you provide the raw value when you define the enumeration.

For example, consider the case of requesting the sunrise and sunset time from a server. The server either responds with the information or it responds with some error information. 
*/

enum ServerResponse {
    
    case successCase(String, String)
    case failure(String)
    
}

let success = ServerResponse.successCase("9.30 AM", "6:30 PM")
let failure = ServerResponse.failure("Failure happened")

switch failure {
    
case let .successCase(start, end):
    print("Hi your working time start at \(start) and will End at \(end)")
    
case let .failure(errorMssg):
    print(errorMssg)
}

//-- 
 // Smaple Rect structure
struct Point {
    var x = 0.0, y = 0.0
}

struct Size {
    var width = 0.0, height = 0.0
}


struct Rect {
    
    var origin = Point() // Property 1
    var size = Size() // P 2
    
    var center : Point {  // P 3
        
        // Getter method
        get {
            
            let centerX = origin.x + (size.width/2)
            let centerY = origin.y + (size.height/2)
            
            return Point(x: centerX, y: centerY)
        }
        
        set(newCenter) {
            
            origin.x = newCenter.x - (size.width/2)
            origin.y = newCenter.y - (size.height/2)
        }
    }
    
}


var squareRect = Rect(origin: Point(x: 10, y: 5), size: Size(width: 44, height: 20))
squareRect.center.x = 50
//squareRect.center.y = 100

squareRect.origin.x
squareRect.center.y

///////////------------ Protocols and Extensions ------------- //////////////

protocol ExampleProtocol {
    
    var simpleDescription: String {get}
    mutating func descriptionModify()
}

//  Classes, enumerations, and structs can all adopt protocols.

//class SampleClass: ExampleProtocol {
//    
//    var sampleClassDescription : String = "This is Sample Class description."
//    var secondProperty : String = "This for testing"
//    
//    func descriptionModify() {
//        sampleClassDescription += " This is additional texts"
//    }
//}

//var sampleObject = SampleClass()
//sampleObject.descriptionModify()
//sampleObject.sampleClassDescription


protocol MyProtocol {
    
    mutating func scale(value: Int)
    
}

struct TestClass : MyProtocol {
    var name : String = "Test"
    var width : Int = 34
    var height : Int = 45
    
    mutating func scale(value: Int) {
        
        width *= value;
        height *= value;
    }
    
}

var test = TestClass()
test.scale(2)
test.width


enum Months : MyProtocol {
    
    case Jan, Feb, March
    
    // Method
   mutating func scale(value: Int) {
    
        
        switch self {
            case .Feb:
                print("FEBRUARY");
            case .March:
                print("MARCH");
            case .Jan:
                print("JANUARY");
            
        }
    }
}

var month = Months.March
month.scale(2)













